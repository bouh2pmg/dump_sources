%include fedora-kickstarts/fedora-live-xfce.ks
%include fedora-live-epitech-packages.ks
%include fedora-live-epitech-blih.ks

keyboard fr
lang en_US.UTF-8
timezone Europe/Paris --isUtc
selinux --permissive
shutdown

%post

echo 'fastestmirror=true' >> /etc/dnf/dnf.conf

%end
