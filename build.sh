#!/bin/bash

command -v ksflatten >/dev/null 2>&1 || { echo >&2 "'ksflatten' is required to proceed.  Aborting."; exit 1; }
command -v livemedia-creator >/dev/null 2>&1 || { echo >&2 "'livemedia-creator' is required to proceed.  Aborting."; exit 1; }

if [ -d fedora-kickstarts ]; then
    echo "'fedora-kickstarts' folder exist, bypass downloading it..."
else
    git clone --single-branch -b f28 https://pagure.io/fedora-kickstarts.git fedora-kickstarts
    if [ $? -ne 0 ]; then
	echo "Failure while cloning fedora-kickstarts" >&2
	exit 1
    fi
fi

ksflatten -c fedora-live-epitech.ks -o flat_fedora-live-epitech.ks --version F28

sed -i 's/^part \/ --fstype="ext4" --size=5120$/part \/ --fstype="ext4" --size=10240/' flat_fedora-live-epitech.ks

sudo livecd-creator --config=flat_fedora-live-epitech.ks --fslabel=Fedora-28-LiveCD-Epitech
